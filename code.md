```
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.core.text.TextUtilsCompat
import java.util.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MainActivity", "onCreate called")
        setContent {
            MyBusinessCardTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MyBusinessCard(
                        name = getString(R.string.my_name),
                        occupation = getString(R.string.my_occupation),
                        phone = stringResource(R.string.phone),
                        nickname = stringResource(R.string.nickname),
                        email = stringResource(R.string.email)
                    )
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("MainActivity", "onStart called")
    }

    override fun onResume() {
        super.onResume()
        Log.d("MainActivity", "onResume called")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MainActivity", "onPause called")
    }

    override fun onStop() {
        super.onStop()
        Log.d("MainActivity", "onStop called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MainActivity", "onDestroy called")
    }
}
```

## Обоснование результата:

1. Когда приложение запускается, вызывается метод onCreate(), затем onStart() и onResume(). Это обычная последовательность вызовов при старте активити.

2. При повороте экрана активити уничтожается, вызывая методы onPause(), onStop() и onDestroy(), а затем пересоздается, вызывая методы onCreate(), onStart() и onResume()

3. Когда сворачиваем приложение, вызывается метод onPause(), что объясняется уходом активити в фоновый режим.

4. Добавление вызова метода finish() в onCreate() приводит к завершению активити после ее создания, что вызывает методы onPause(), onStop() и onDestroy(), так как активити завершается.


- onStart(), onStop() - отвечают за визуальное отображение активити пользователю
- onResume(), onPause() - за помещение активити в фокус (возможность/невозможность взаимодействовать)
- finish() - завершение текущей активити, после него вызывается метод onDestroy() и др действия
- onCreate(), onDestroy() - создание и уничтожение активити

